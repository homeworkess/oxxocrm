<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="screen-options-area"></div>
    <div class="content">
        <div class="row">

            <?php hooks()->do_action('before_start_render_dashboard_content'); ?>

            <div class="clearfix"></div>

            <div class="col-md-12 mtop30" data-container="top-12">

            </div>

            <div class="clearfix"></div>

        </div>

        <div class="row">
            <h3 class="text-center">Status</h3>
            <hr/>

            <div class="flx-container">
                <div class="status-item">
                    <h3>Online</h3>
                    <h2 id="st_online">N/A</h2>
                </div>

                <div class="status-item">
                    <h3>Offline</h3>
                    <h2 id="st_offline">N/A</h2>
                </div>

                <div class="status-item">
                    <h3>Unreachable</h3>
                    <h2 id="st_unreachable">N/A</h2>
                </div>

                <div class="status-item">
                    <h3>Pending</h3>
                    <h2 id="st_pending">N/A</h2>
                </div>
            </div>

        </div>

        <div class="row text-center">
            <h3>Services</h3>
            <hr/>

            <div class="flx-container">
                <div class="status-item">
                    <h3>OK</h3>
                    <h2 id="sr_ok">N/A</h2>
                </div>

                <div class="status-item">
                    <h3>Warning</h3>
                    <h2 id="sr_warning">N/A</h2>
                </div>

                <div class="status-item">
                    <h3>Critical</h3>
                    <h2 id="sr_critical">N/A</h2>
                </div>

                <div class="status-item">
                    <h3>Unknown</h3>
                    <h2 id="sr_unknown">N/A</h2>
                </div>

                <div class="status-item">
                    <h3>Pending</h3>
                    <h2 id="sr_pending">N/A</h2>
                </div>

            </div>


        </div>

    </div>
</div>

<?php init_tail(); ?>
<?php $this->load->view('admin/utilities/calendar_template'); ?>
<?php $this->load->view('admin/dashboard/dashboard_js'); ?>

<style type="text/css">

    .flx-container {
        display: flex;
        flex-flow: row;
        justify-content: space-evenly;
    }

    .status-item {
        background: white;
        height: 250px;
        border-radius: 10px;
        margin: 1em;
        padding: 1em;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        flex: 1 1 0px;
    }


</style>

<script type="text/javascript">


    (async() => {

        // Setup ajax call for the nagios api
        const headers = new Headers();
        const params = {method: 'GET', headers, mode: 'cors', cache: 'default'};
        let timeOut = 5000;


        let timeInterval = setInterval(async()=>{

            const request = await fetch("<?= admin_url('nagios/status/refresh') ?>", params);
            const data = await request.json();

            Object.entries({...data.services,...data.status}).forEach(
                ([key, value]) => {
                    document.getElementById(key).innerHTML = value;
                }
            );

        }, timeOut);




    })()



</script>


</body>
</html>
