<?php

/**
 * Ensures that the module init file can't be accessed directly, only within the application.
 */
defined('BASEPATH') or exit('No direct script access allowed');

/*
Module Name: Proxidesk - Nagios Report
Description: Module de reporting Nagios.
Version: 0.1
Requires at least: 2.3.*
*/




hooks()->add_action('admin_init', 'nagios_menu_item_collapsible');
hooks()->add_action('before_start_render_dashboard_content', 'before_start_render_dashboard_content');

function nagios_menu_item_collapsible()
{
    $CI = &get_instance();

    $CI->app_menu->add_sidebar_menu_item('custom-menu-unique-id', [
        'name'     => 'Nagios', // The name if the item
        'collapse' => true, // Indicates that this item will have submitems
        'position' => 2, // The menu position
        'icon'     => 'fa fa-question-circle', // Font awesome icon
    ]);

    // The first paremeter is the parent menu ID/Slug
    $CI->app_menu->add_sidebar_children_item('custom-menu-unique-id', [
        'slug'     => 'child-to-custom-menu-item', // Required ID/slug UNIQUE for the child menu
        'name'     => 'Statut', // The name if the item
        'href'     => admin_url('nagios/status/index'), // URL of the item
        'position' => 5, // The menu position
    ]);

    // Parameters Menu
    $CI->app_menu->add_sidebar_children_item('custom-menu-unique-id', [
        'slug'     => 'child-to-custom-menu-item', // Required ID/slug UNIQUE for the child menu
        'name'     => 'Paramètres', // The name if the item
        'href'     => admin_url('nagios/status/configuration'), // URL of the item
        'position' => 5, // The menu position
    ]);

}

function before_start_render_dashboard_content(){

    return '<h1>Hook</h1>';
}
