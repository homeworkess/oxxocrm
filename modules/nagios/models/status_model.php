<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Status_model extends App_Model
{
    private $nag_url = "http://185.43.38.4/nagios/";
    private $nag_usr = "nagiosadmin";
    private $nag_pwd = "YAgs4JJHjsWUPP";

    public function __construct()
    {
        parent::__construct();
    }

    private function fetchServicesStatus($get_url)
    {
        global $nag_url, $nag_usr, $nag_pwd;
        $get_status = curl_init($nag_url . $get_url);
        curl_setopt($get_status, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($get_status, CURLOPT_USERPWD, $nag_usr . ":" . $nag_pwd);
        $res = curl_exec($get_status);
        curl_close($get_status);
        return json_decode($res, true);
    }

    public function fetchCurrentServiceStatus(): array
    {
        //Get statistics
        $statsHosts = $this->fetchServicesStatus("cgi-bin/statusjson.cgi?query=hostcount");
        $statsServices = $this->fetchServicesStatus("cgi-bin/statusjson.cgi?query=servicecount");

        return [
            'status' => [
                'st_online' => $statsHosts["data"]["count"]["up"],
                'st_offline' => $statsHosts["data"]["count"]["down"],
                'st_unreachable' => $statsHosts["data"]["count"]["unreachable"],
                'st_pending' => $statsHosts["data"]["count"]["pending"],
            ],
            'services' => [
                'sr_ok' => $statsServices["data"]["count"]["ok"],
                'sr_warning' => $statsServices["data"]["count"]["warning"],
                'sr_critical' => $statsServices["data"]["count"]["critical"],
                'sr_unknown' => $statsServices["data"]["count"]["unknown"],
                'sr_pending' => $statsServices["data"]["count"]["pending"],
            ]
        ];


    }

}
