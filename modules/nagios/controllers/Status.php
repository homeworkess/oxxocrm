<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Status extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /* List all announcements */
    public function index()
    {
        $this->load->view('index', []);
    }

    public function refresh()
    {
        $this->load->model('status_model');

        if ($this->input->is_ajax_request()) {

            return $this->output
                ->set_content_type('application/json')
                ->set_status_header(200)
                ->set_output(json_encode($this->status_model->fetchCurrentServiceStatus()));
        }

    }

    public function configuration()
    {
        echo "configuration";
    }

}
