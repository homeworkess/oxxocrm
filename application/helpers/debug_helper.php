<?php

if(!function_exists('debug'))
{
    function debug($value, bool $stop=false)
    {
        echo '<pre>';
        print_r($value);
        echo '</pre>';
        if ($stop) exit;
    }
}
